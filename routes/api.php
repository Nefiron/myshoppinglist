<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Shopping List Routes
Route::get('/list', 'ShoppingListController@index');
Route::post('/list', 'ShoppingListController@store');
Route::put('/list/item/{id}', 'ShoppingListController@updateStatus');
Route::delete('/list/item/{id}', 'ShoppingListController@destroy');