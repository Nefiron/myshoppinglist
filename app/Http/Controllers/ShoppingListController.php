<?php

namespace App\Http\Controllers;

use App\ShoppingList;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ShoppingListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all items from list
        $list = ShoppingList::all();

        return response(['status' => 'success', 'data' => $list], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store item in list
        $item = ShoppingList::create([
            'description' => $request->description,
            'price' => $request->price,
            'completed' => 0
        ]);

        return response(['status' => 'success', 'data' => $item], 200);
    }

    /**
     * Update shopping list item status and persist state.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, $id)
    {
        // Find item by id
        $item = ShoppingList::find($id);

        // Update item properties
        if ($item->completed == 0) {
            $item->completed = 1;
        } else if ($item->completed == 1) {
            $item->completed = 0;
        }

        $item->save();

        return response(['status' => 'success', 'data' => $item], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShoppingList  $shoppingList
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Destroy item in list
        $item = ShoppingList::find($id);

        $item->delete();
        
        return response(['success' => 'true']);
    }
}
